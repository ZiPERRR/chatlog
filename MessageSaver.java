public class MessageSaver{
    String memberName;
    String userText;
    String userId;
    String guildId;
    String textChannelId;
    String messageId;

    public MessageSaver(String memberName, String userText, String userId, String guildId, String textChannelId,
                        String messageId) {

        this.memberName = memberName;
        this.userText = userText;
        this.userId = userId;
        this.guildId = guildId;
        this.textChannelId = textChannelId;
        this.messageId = messageId;
    }
}
