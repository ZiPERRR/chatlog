import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.jetbrains.annotations.NotNull;
import javax.security.auth.login.LoginException;
import java.io.FileWriter;
import java.io.IOException;

public class DiscordBot extends ListenerAdapter  {

    public static void main(String[] args) throws LoginException, InterruptedException{


        JDA jda = JDABuilder.create("", GatewayIntent.getIntents(GatewayIntent.DEFAULT))
                .setToken("YOUR TOKEN")
                .addEventListeners(new DiscordBot())
                .build().awaitReady();

    }

    @Override
    public void onMessageReceived(@NotNull MessageReceivedEvent event) {
        super.onMessageReceived(event);

        String memberName = event.getAuthor().getName();
        String userText = event.getMessage().getContentRaw();
        String userId = event.getAuthor().getId();
        String guildId = event.getGuild().getId();
        String textChannelId = event.getTextChannel().getId();
        String messageId = event.getMessage().getId();


        try {
            userMessageToGSON(memberName, userText, userId, guildId, textChannelId, messageId);
        } catch (IOException e) {
            System.out.println("IOException error");
        }

    }

    public void userMessageToGSON(String memberName, String userText, String userId, String guildId, String textChannelId,
                                  String messageId)
            throws IOException {

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        FileWriter writer = new FileWriter("ENTER YOUR PATH AND NAME.json HERE", true);
        MessageSaver messageSaver = new MessageSaver(memberName, userText, userId, guildId, textChannelId, messageId);
        gson.toJson(messageSaver, writer);
        writer.close();
    }
}